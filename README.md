
La convocatoria titulada “Potenciar el reciclaje con inclusión social”, reunió a más de 20 colaboradores, promotores y mediadores que trabajaron en torno a la creación de una aplicación móvil y al diseño de contenedores de acopio temporal de materiales reciclables.

El laboratorio, desarrollado en modalidad virtual los días 3, 4 y 5 de diciembre, se nutrió de aportes de los distintos participantes que supieron propiciar en todo momento el trabajo colectivo en un marco de respeto y escucha activa. Es para destacar la empatía y la búsqueda por el bien común como metas centrales de cada uno de los proyectos abordados.
